//
//  Chat.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 20.03.2022.
//

import Foundation

struct Chat: Identifiable {
    let id = UUID()
    var chatOwners: [User]
    var messages: [Message]
}

extension Chat {
    static let sampleData: [Chat] = [dummy1, dummy2]
    
    static var dummy1: Chat {
        Chat(chatOwners: [User.dummy1, User.dummy2], messages: Message.sampleData1)
    }
    
    static var dummy2: Chat {
        Chat(chatOwners: [User.dummy1, User.dummy3], messages: Message.sampleData2)
    }
}

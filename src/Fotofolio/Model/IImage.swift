//
//  IdentifiableImage.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 08.03.2022.
//

import Foundation
import SwiftUI

struct IImage: Identifiable {
    let id = UUID()
    var src: MyImageEnum
}

enum MyImageEnum {
    case remote(String)
    case local(Image)
}

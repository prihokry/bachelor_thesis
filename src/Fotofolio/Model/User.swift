//
//  Creator.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 03.02.2022.
//

import Foundation

let PROFILE_PIC = "https://placeimg.com/320/320/person"

struct User: Identifiable {
    let id: UUID
    var username: String
    var fullName: String
    var password: Int
    var email: String
    var location: String
    var profilePicture: IImage?
    var ratings: [String:Int]
    var creator: Creator?
    
    func calculateRating() -> Double {
        if ratings.isEmpty { return 0 }
        
        let sum = ratings.values.reduce(0, +)
        return Double(sum) / Double(ratings.count)
    }
    
    func isCreator() -> Bool {
        return creator != nil
    }
}

extension User {
    static let sampleData: [User] = [dummy1, dummy2, dummy3, dummy4, dummy5, dummy6]
    
    static var dummy1: User {
        User(id: UUID(), username: "vojtafoti", fullName: "Vojta Votruba", password: "heslovo".hashValue, email: "vojtano@mail.com", location: "Praha",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: ["ad.fotograf": 5, "michal1": 4], creator: Creator.dummy1)
    }
    
    static var dummy2: User {
        User(id: UUID(), username: "ad.fotograf", fullName: "Adam Lupínek", password: "hesload".hashValue, email: "adam@mail.cz", location: "Kladno",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: ["eatinkriss": 5], creator: Creator.dummy2)
    }
    
    static var dummy3: User {
        User(id: UUID(), username: "karel__foti", fullName: "Karel Kovář", password: "hesloka".hashValue, email: "karel@mail.com", location: "Buštěhrad",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: ["milos": 2], creator: Creator.dummy3)
    }
    
    static var dummy4: User {
        User(id: UUID(), username: "majkl_98", fullName: "Michal Filip", password: "hesloma".hashValue, email: "mike@mail.cz", location: "Kolín",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: [:])
    }
    
    static var dummy5: User {
        User(id: UUID(), username: "nejfotograf", fullName: "Miroslav Koch", password: "heslone".hashValue, email: "mira@mail.cz", location: "Velké Karlovice",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: [:], creator: Creator.dummy4)
    }
    
    static var dummy6: User {
        User(id: UUID(), username: "portretyodmilana", fullName: "Milan Drát", password: "heslosv".hashValue, email: "milan@mail.cz", location: "Velvary",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: ["majkl_98": 5], creator:
             Creator.dummy5)
    }
    
    static var dummy7: User {
        User(id: UUID(), username: "fotimcvakam", fullName: "Radek Kříž", password: "heslofo".hashValue, email: "fotcvakam@mail.cz", location: "Praha",
             profilePicture: IImage(src: .remote(PROFILE_PIC)), ratings: [:])
    }
}

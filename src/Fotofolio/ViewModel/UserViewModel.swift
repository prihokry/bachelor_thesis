//
//  UserViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 03.02.2022.
//

import Foundation

class UserViewModel: ObservableObject {
    @Published var users: [User] = User.sampleData
    
    func getUserByUsername(_ username: String) -> User? {
        return users.first(where: {
            $0.username == username
        })
    }
}

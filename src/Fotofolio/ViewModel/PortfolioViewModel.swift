//
//  PortfolioViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 03.02.2022.
//

import Foundation
import SwiftUI

class PortfolioViewModel: ObservableObject {
    @Published var portfolios: [Portfolio] = []
    @Published var flagged: [Portfolio] = []
    @Published var tags: [String] = []
    
    @ObservedObject var userViewModel = UserViewModel()
    
    func fetchPortfolios() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            withAnimation {
                self.portfolios = Portfolio.sampleData
            }
        }
    }
    
    func addToFlagged(_ portfolio: Portfolio) {
        if (!flagged.contains(portfolio)) {
            self.flagged.append(portfolio)
        }
    }
    
    func removeFromFlagged(_ portfolio: Portfolio) {
        self.flagged.removeAll(where: { $0 == portfolio })
    }
    
    func clearFlagged() {
        self.flagged = []
    }
    
    func sortPortfoliosByDate() {
        self.portfolios = self.portfolios.sorted(by: { $0.timestamp > $1.timestamp })
    }
    
    func sortPortfoliosByRating() {
        self.portfolios = self.portfolios.sorted(by: {
            let first = userViewModel.getUserByUsername($0.authorUsername)?.calculateRating()
            let second = userViewModel.getUserByUsername($1.authorUsername)?.calculateRating()
            
            guard let first = first else { return true }
            guard let second = second else { return true }
            
            return first > second
        })
    }
    
    func addTag(_ tag: String) -> Bool {
        if (!tag.isEmpty && !tags.contains(tag)) {
            self.tags.append(tag.lowercased())
            
            return true
        }
        return false
    }
    
    func removeTag(_ tag: String) {
        self.tags.removeAll(where: { $0 == tag })
    }
    
    func filterPortfolios() {
        var tmpPortfolios = [Portfolio]()
        
        tags.forEach({ tag in
            let tmp = portfolios.filter({ pfolio in
                pfolio.tags.contains(tag)
            })
            
            tmp.forEach({ pfolio in
                if !tmpPortfolios.contains(pfolio) {
                    tmpPortfolios.append(pfolio)
                }
            })
        })
        
        portfolios = tmpPortfolios
    }
    
    func reset() {
        flagged = []
        tags = []
    }
}

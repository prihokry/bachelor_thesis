//
//  ProfileViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 09.02.2022.
//

import Foundation
import SwiftUI

class ProfileViewModel: ObservableObject {
    @Published var user: User?
    @Published var portfolios: [Portfolio] = []
    
    @ObservedObject var userViewModel: UserViewModel = .init()
    @ObservedObject var portfolioViewModel: PortfolioViewModel = .init()
    
    @Published var fetchingInfo: Bool = true
    @Published var fetchingPortfolios: Bool = true
    
    func fetchProfileInfo(username: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.user = self.userViewModel.users.first(where: { $0.username == username })
            self.fetchingInfo = false
        }
    }
    
    func fetchProfilePortofolios(username: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.portfolios = Portfolio.sampleData.filter { $0.authorUsername == username }
            self.fetchingPortfolios = false
        }
    }
    
    func reset() {
        user = nil
        portfolios = []
    }
}

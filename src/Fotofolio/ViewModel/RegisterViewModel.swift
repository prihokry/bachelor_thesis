//
//  RegisterViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 18.03.2022.
//

import Foundation
import UIKit

class RegisterViewModel: ObservableObject {
    @Published var username = ""
    @Published var fullName = ""
    @Published var email = ""
    @Published var password = ""
    @Published var passwordConfirmation = ""
    @Published var location = ""
    @Published var profilePicture: IImage?
    
    @Published var fillNameAlert: Bool = false
    @Published var fillEmailAlert: Bool = false
    @Published var usernameAlert: Bool = false
    @Published var fillPasswordAlert: Bool = false
    @Published var fillLocationAlert: Bool = false
    
    @Published var userNameExists = false
    @Published var notAnEmail = false
    @Published var badUsernameCharacters = false
    
    @Published var dataValidated = false
    
    //image picker properties
    @Published var newProfileImage: UIImage?
    @Published var isPickerPresented = false
    
    func containsNumbers(_ str: String) -> Bool {
        let numbersRange = str.rangeOfCharacter(from: .decimalDigits)
        return (numbersRange != nil)
    }
    
    func createUser() -> User {
        return User(id: UUID(), username: username, fullName: fullName, password: password.hashValue, email: email,
                       location: location, profilePicture: profilePicture, ratings: [:], creator: nil)
    }
}

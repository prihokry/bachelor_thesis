//
//  SignInViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 14.03.2022.
//

import Foundation
import SwiftUI

class SignInViewModel: ObservableObject {
    @AppStorage("username") var username: String = "default"
    
    @Published var unauthorized: Bool = true
    
    @Published var authorizingUsername: Bool = true
    @Published var authorizingPassword: Bool = true
    
    @Published var userExists: Bool = false
    @Published var passwordCorrect: Bool = false
    
    @ObservedObject var userViewModel: UserViewModel = .init()
    
    func authorizeUser(username: String, password: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.unauthorized = !self.userViewModel.users.contains(where: {
                $0.username == username && $0.password == password.hashValue
            })
        }
    }
    
    func userRegistered(usernameInput: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.userExists = self.userViewModel.users.contains(where: { $0.username == usernameInput })
            self.authorizingUsername = false
        }
    }
    
    func isPasswordCorrect(usernameInput: String, passwordInput: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.passwordCorrect = self.userViewModel.users.contains(where: { $0.username == usernameInput && $0.password == passwordInput.hashValue })
            self.authorizingPassword = false
        }
    }
    
    func isSignedIn() {
        if username != "default" {
            unauthorized = false
        }
    }
    
    func isAnEmailAddress(_ email: String) -> Bool {
        let emailRegex = NSPredicate(format: "SELF MATCHES %@", "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
        return emailRegex.evaluate(with: email)
    }
    
    func isUsername(_ str: String) -> Bool {
        let usernameRegex = NSPredicate(format: "SELF MATCHES %@", "^[A-Za-z0-9_]*$")
        return usernameRegex.evaluate(with: str)
    }
    
    func reset() {
        userExists = false
        passwordCorrect = false
        authorizingUsername = true
        authorizingPassword = true
    }
}

//
//  MessagesViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 20.03.2022.
//

import Foundation
import SwiftUI

class MessagesViewModel: ObservableObject {
    @Published var chats: [Chat] = []
    
    @Published var fetchingChats: Bool = true
    
    func fetchChats(for username: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.13) {
            self.chats = Chat.sampleData.filter({
                $0.chatOwners.contains(where: {
                    $0.username == username
                })
            })
            
            self.fetchingChats = false
        }
    }
}

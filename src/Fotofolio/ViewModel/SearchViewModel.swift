//
//  SearchViewModel.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 21.02.2022.
//

import Foundation

class SearchViewModel: ObservableObject {
    @Published var searchedUsers: [User] = []
    
    @Published var searching = false
    @Published var searchMode = 0 //search by username
    @Published var searchString = ""
    
    let searchOptions = ["Uživatelské jméno", "Poloha"]
    
    func fetchUsersBySubstring(input searchString: String, mode searchMode: Int = 0) -> [User] {
        var current: [User] = []
        
        if searchMode == 0 {
            current = User.sampleData.filter({
                $0.username.contains(searchString.lowercased())
            })
        } else {
            current = User.sampleData.filter({
                $0.location.lowercased().contains(searchString.lowercased())
            })
        }
        
        return current
    }
}

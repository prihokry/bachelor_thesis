//
//  PortfolioDetailView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 30.01.2022.
//

import SwiftUI

struct PortfolioDetailView: View {
    @StateObject var userViewModel: UserViewModel = .init()
    
    @EnvironmentObject var portfolioViewModel: PortfolioViewModel
    var portfolio: Portfolio
    
    @State var portfolioAuthor: User?
    
    @State private var removePortfolio = false
    
    var body: some View {
        VStack {
            if let author = portfolioAuthor {
                AuthorPortfolioView(author: author, removePortfolio: $removePortfolio)
            } else {
                ProgressView()
                    .padding()
            }
            
            PhotoCarouselView(photos: portfolio.photos)
            
            HStack {
                Text(portfolio.description)
                    .font(.system(size: 16))
                    .foregroundColor(Color(uiColor: UIColor.systemGray))
                
                Spacer()
            }
            .padding(.top, 5)
            .padding(.leading, 21)
            .padding(.trailing, 21)
        }
        .padding(.bottom, 10)
        .alert("Opravdu chcete odstranit portfolio z výběru?", isPresented: $removePortfolio) {
            Button("Ano") {
                portfolioViewModel.removeFromFlagged(portfolio)
            }
            .foregroundColor(.red)
            
            Button("Ne") { }
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.65) {
                portfolioAuthor = userViewModel.getUserByUsername(portfolio.authorUsername)
            }
        }
    }
}

struct PortfolioDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioDetailView(portfolioViewModel: .init(), portfolio: Portfolio.dummyPortfolio4)
    }
}

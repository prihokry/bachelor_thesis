//
//  AuthorPortfolioView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 09.02.2022.
//

import SwiftUI

struct AuthorPortfolioView: View {
    var author: User
    @Binding var removePortfolio: Bool
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {}, label: {
                    NavigationLink(destination: ProfileView(profileViewModel: .init(), user: author.username, signOutClear: .constant(false)), label: {
                        Text("@\(author.username)")
                            .font(.title3)
                            .padding(.leading, 20)
                            .foregroundColor(.pink)
                    })
                })
                
                Spacer()
                
                Button(action: {}) {
                    NavigationLink(destination: ChatView(receiver: author)) {
                        Text("Napsat zprávu")
                            .foregroundColor(.cyan)
                    }
                }
                
                Button(action: {
                    removePortfolio = true
                }) {
                    Image(systemName: "bookmark.slash")
                        .foregroundColor(.black)
                }
                .padding(.trailing, 20)
            }
            .padding(.bottom, 1)
            
            HStack {
                VStack(alignment: .leading) {
                    Text("\(author.fullName)")
                        .foregroundColor(.black).brightness(0.3)
                        .font(.system(size: 16))
                    
                    if !author.ratings.isEmpty {
                        HStack {
                            Text("\(author.location), " + String(format: "%.1f", author.calculateRating()) + " z 5")
                                .font(.system(size: 12))
                                .foregroundColor(.black).brightness(0.3)
                            
                            Image(systemName: "star.fill")
                                .font(.system(size: 8))
                                .foregroundColor(.black).brightness(0.3)
                                .offset(x: -5)
                        }
                    } else {
                        Text("\(author.location), " + "Bez hodnocení")
                            .font(.system(size: 12))
                            .foregroundColor(.black).brightness(0.3)
                    }
                }
                .padding(.leading, 25)
                
                Spacer()
            }
            
            HStack {
                Text(author.creator?.profileText ?? "Profilový popis se nepodařilo nahrát, zkuste to znovu.")
                    .font(.system(size: 16))
                    .foregroundColor(Color(uiColor: UIColor.systemGray))
                
                Spacer()
            }
            .padding(.top, 5)
            .padding(.leading, 21)
            .padding(.trailing, 21)
        }
    }
}

struct AuthorPortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        AuthorPortfolioView(author: User.dummy6, removePortfolio: .constant(true))
    }
}

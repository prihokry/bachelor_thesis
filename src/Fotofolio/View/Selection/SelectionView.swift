//
//  SelectionView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 28.01.2022.
//

import SwiftUI

struct SelectionView: View {
    @EnvironmentObject var portfolioViewModel: PortfolioViewModel
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    
    @AppStorage("username") var username: String = "default"
    
    @State private var clearSelection = false
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                if (!portfolioViewModel.flagged.isEmpty) {
                    ForEach(portfolioViewModel.flagged, id: \.id) { portfolio in
                        PortfolioDetailView(portfolio: portfolio)
                        
                        if portfolioViewModel.flagged.last != portfolio {
                            Divider()
                                .padding(.leading, 18)
                                .padding(.trailing, 18)
                                .padding(.bottom, 10)
                        }
                    }
                } else {
                    Text("Není co zobrazovat, vyberte si portfolia ve Feedu!")
                        .frame(width: geo.size.width, height: geo.size.height)
                        .font(.system(size: 15))
                        .foregroundColor(.gray)
                }
            }
        }
        .navigationTitle("Výběr")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                if !portfolioViewModel.flagged.isEmpty {
                    Button(action: {
                        clearSelection = true
                    }) {
                        Text("Odstranit vše")
                            .foregroundColor(.gray)
                    }
                    .padding(.trailing, 5)
                }
            }
        }
        .onAppear {
            if messagesViewModel.chats.isEmpty {
                messagesViewModel.fetchChats(for: username)
            }
        }
        .alert("Opravdu chcete odstranit všechna portfolia ze svého výběru?", isPresented: $clearSelection) {
            Button("Ano") {
                portfolioViewModel.clearFlagged()
            }
            
            Button("Ne") { }
        }
    }
}

struct SelectionView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SelectionView()
                .environmentObject(PortfolioViewModel.init())
                .environmentObject(MessagesViewModel.init())
        }
    }
}

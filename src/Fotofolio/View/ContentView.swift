//
//  ContentView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 27.01.2022.
//

import SwiftUI

struct ContentView: View {
    @StateObject var portfolioViewModel: PortfolioViewModel = .init()
    @StateObject var messagesViewModel: MessagesViewModel = .init()
    @StateObject var profileViewModel: ProfileViewModel = .init()
    @StateObject var signInViewModel: SignInViewModel = .init()
    @StateObject var userViewModel: UserViewModel = .init()
    
    @State var tabSelection = TabEnum.feed
    @State var signOutClear = false
    
    var body: some View {
        TabView(selection: $tabSelection) {
            NavigationView {
                FeedView()
            }
            .tabItem {
                Image(systemName: "square.text.square")
                Text("Feed")
            }
            .tag(TabEnum.feed)
            
            NavigationView {
                SelectionView()
            }
            .badge(portfolioViewModel.flagged.count)
            .tabItem {
                Image(systemName: "star.square")
                Text("Výběr")
            }
            .tag(TabEnum.selection)
            
            NavigationView {
                SearchView()
            }
            .tabItem {
                Image(systemName: "magnifyingglass")
                Text("Vyhledávání")
            }
            .tag(TabEnum.search)
            
            NavigationView {
                MessagesView()
            }
            .navigationViewStyle(StackNavigationViewStyle())
            .tabItem {
                Image(systemName: "message")
                Text("Zprávy")
            }
            .tag(TabEnum.messages)
            
            NavigationView {
                ProfileView(profileViewModel: profileViewModel, user: signInViewModel.username,
                            profileOwner: true, signOutClear: $signOutClear)
            }
            .tabItem {
                Image(systemName: "person.crop.square")
                Text("Můj profil")
            }
            .tag(TabEnum.profile)
        }
        .fullScreenCover(isPresented: $signInViewModel.unauthorized, content: {
            SignInView()
        })
        .onChange(of: signOutClear) { _ in
            if signOutClear { resetEnvironment() }
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .environmentObject(portfolioViewModel)
        .environmentObject(messagesViewModel)
        .environmentObject(profileViewModel)
        .environmentObject(signInViewModel)
        .environmentObject(userViewModel)
    }
    
    func resetEnvironment() {
        signInViewModel.unauthorized = true
        signInViewModel.reset()
        
        portfolioViewModel.reset()
        
        tabSelection = .feed
        
        profileViewModel.reset()
        messagesViewModel.chats = []
        
        signOutClear = false
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ContentView()
        }
    }
}

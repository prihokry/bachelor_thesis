//
//  RegisterView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 15.03.2022.
//

import SwiftUI

struct RegisterUserView: View {
    @EnvironmentObject var signInViewModel: SignInViewModel
    @EnvironmentObject var profileViewModel: ProfileViewModel
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    @EnvironmentObject var userViewModel: UserViewModel
    @StateObject var registerViewModel: RegisterViewModel = .init()
    
    @Binding var rootActive: Bool
    @Binding var signInUsername: String
    @Binding var registered: Bool
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            NavigationLink(isActive: $registerViewModel.dataValidated,
                           destination: { UserTypeView(rootActive: $rootActive, signInUsername: $signInUsername, registered: $registered) },
                           label: { EmptyView() })
                .isDetailLink(false)
            
            //Profile picture
            ZStack {
                if let pic = registerViewModel.profilePicture {
                    if case MyImageEnum.local(let img) = pic.src {
                        img
                            .resizable()
                            .frame(width: 120, height: 120, alignment: .leading)
                            .cornerRadius(100)
                            .overlay(content: {
                                Circle()
                                    .fill(.gray)
                                    .frame(width: 120, height: 120, alignment: .leading)
                                    .opacity(0.5)
                            })
                    }
                } else {
                    Circle()
                        .fill(.gray).brightness(0.35)
                        .frame(width: 120, height: 120, alignment: .leading)
                }
                
                Button(action: { registerViewModel.isPickerPresented = true }) {
                    Image(systemName: "pencil")
                        .foregroundColor(.black)
                }
            }
            .padding(.bottom, 25)
            .padding(.top, 10)
            
            //Full name
            VStack {
                Text("Jméno a příjmení")
                    .brightness(0.27)
                
                TextField("Jméno Příjmení", text: $registerViewModel.fullName)
                    .font(.system(size: 18))
                    .frame(height: 38)
                    .offset(x: 9)
                    .padding()
                    .background(.gray).brightness(0.35)
                    .cornerRadius(9)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .overlay(
                        RoundedRectangle(cornerRadius: 9)
                            .stroke(.red, lineWidth: 1)
                            .opacity(registerViewModel.fillNameAlert ? 1 : 0)
                    )
            }
            
            //Email
            VStack {
                Text("Email")
                    .brightness(0.27)
                
                TextField("Email", text: $registerViewModel.email)
                    .font(.system(size: 18))
                    .frame(height: 38)
                    .offset(x: 9)
                    .padding()
                    .background(.gray).brightness(0.35)
                    .cornerRadius(9)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .overlay(
                        RoundedRectangle(cornerRadius: 9)
                            .stroke(.red, lineWidth: 1)
                            .opacity(registerViewModel.fillEmailAlert ? 1 : 0)
                    )
            }
            
            if registerViewModel.notAnEmail {
                Text("Neplatný formát emailové adresy!")
                    .font(.system(size: 14))
                    .foregroundColor(.red).brightness(0.1)
                    .padding(.bottom, 7)
            }
            
            //Username + password
            NewCredentialsView(username: $registerViewModel.username, password: $registerViewModel.password, passwordSecond: $registerViewModel.passwordConfirmation,
                               usernameAlert: $registerViewModel.usernameAlert, userNameExists: $registerViewModel.userNameExists, badUsernameCharacters: $registerViewModel.badUsernameCharacters)
            
            //Location
            VStack {
                Text("Místo působení")
                    .brightness(0.27)
                
                TextField("Místo působení", text: $registerViewModel.location)
                    .font(.system(size: 18))
                    .frame(height: 38)
                    .offset(x: 9)
                    .padding()
                    .background(.gray).brightness(0.35)
                    .cornerRadius(9)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .overlay(
                        RoundedRectangle(cornerRadius: 9)
                            .stroke(.red, lineWidth: 1)
                            .opacity(registerViewModel.fillLocationAlert ? 1 : 0)
                    )
            }
            
            //Button to confirm
            Button(action: {
                if userInputValid() {
                    createUser()
                    registerViewModel.dataValidated = true
                }
            }) {
                Text("Další")
                    .foregroundColor(.white)
                    .padding([.leading, .trailing], 100)
                    .padding([.top, .bottom], 15)
                    .background(.black).brightness(0.13)
                    .cornerRadius(9)
            }
                .padding(.top, 9)
        }
        .padding([.leading, .trailing], 25)
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Registrace")
        .sheet(isPresented: $registerViewModel.isPickerPresented, onDismiss: {
            if let img = registerViewModel.newProfileImage {
                registerViewModel.profilePicture = IImage(src: .local(Image(uiImage: img)))
            }
            registerViewModel.newProfileImage = nil
        }, content: {
            ImagePicker(isPresented: $registerViewModel.isPickerPresented, image: $registerViewModel.newProfileImage)
        })
    }
    
    private func userInputValid() -> Bool {
        var faultyData = false
        
        if registerViewModel.fullName.isEmpty ||
           registerViewModel.containsNumbers(registerViewModel.fullName) {
            registerViewModel.fillNameAlert = true
            faultyData = true
        }
        
        if registerViewModel.email.isEmpty {
            registerViewModel.fillEmailAlert = true
            faultyData = true
        }
        
        if !signInViewModel.isAnEmailAddress(registerViewModel.email) {
            registerViewModel.notAnEmail = true
            faultyData = true
        }
        
        if registerViewModel.username.isEmpty {
            registerViewModel.usernameAlert = true
            faultyData = true
        }
        
        if !signInViewModel.isUsername(registerViewModel.username) {
            registerViewModel.badUsernameCharacters = true
            faultyData = true
        }
        
        if userViewModel.users.contains(where: {$0.username == registerViewModel.username }) {
            registerViewModel.userNameExists = true
            faultyData = true
        }
        
        if registerViewModel.password.isEmpty {
            registerViewModel.fillPasswordAlert = true
            faultyData = true
        }
        
        if registerViewModel.passwordConfirmation.isEmpty {
            registerViewModel.fillPasswordAlert = true
            faultyData = true
        }
        
        if registerViewModel.location.isEmpty {
            registerViewModel.fillLocationAlert = true
            faultyData = true
        }
        
        return !faultyData
    }
    
    private func createUser() {
        let new = registerViewModel.createUser()
        
        //mocking registration of the new user
        profileViewModel.user = new
        signInViewModel.userViewModel.users.append(new)
        profileViewModel.userViewModel.users.append(new)
        userViewModel.users.append(new)
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RegisterUserView(registerViewModel: .init(), rootActive: .constant(false), signInUsername: .constant(""), registered: .constant(true))
                .environmentObject(SignInViewModel())
                .environmentObject(ProfileViewModel())
                .environmentObject(MessagesViewModel())
                .environmentObject(UserViewModel())
        }
    }
}

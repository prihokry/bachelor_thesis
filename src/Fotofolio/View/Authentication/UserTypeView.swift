//
//  UserTypeView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 15.03.2022.
//

import SwiftUI

struct UserTypeView: View {
    @EnvironmentObject var signInViewModel: SignInViewModel
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @Environment(\.dismiss) private var dismiss
    
    @State var isCreator = false
    
    @Binding var rootActive: Bool
    @Binding var signInUsername: String
    
    @Binding var registered: Bool
    
    var body: some View {
        VStack {
            Spacer()
            Text("Tvůrci mohou vytvářet portfolia\na prezentovat svou práci.")
                .font(.system(size: 23))
                .multilineTextAlignment(.leading)
                .padding(.bottom)
                .foregroundColor(.red).brightness(0.3)
            
            Text(isCreator ? "Nemůžeme se dočkat na Vaše portfolia!" : "Staňte se tvůrcem!")
            
            Toggle("", isOn: $isCreator)
                .labelsHidden()
            
            Spacer()
            
            if isCreator {
                Button(action: { }, label: {
                    NavigationLink(destination: { RegisterCreator(rootActive: $rootActive, registered: $registered, signInUsername: $signInUsername) },
                                   label: {
                        Text("Další")
                            .foregroundColor(.white)
                            .padding([.leading, .trailing], 100)
                            .padding([.top, .bottom], 15)
                            .background(.black)
                            .cornerRadius(9)
                    })
                        .isDetailLink(false)
                })
            } else {
                Button(action: {
                    signInUsername = profileViewModel.user?.username ?? ""
                    registered = true
                    rootActive = false
                }, label: {
                    Text("Registrovat")
                        .foregroundColor(.white)
                        .padding([.leading, .trailing], 100)
                        .padding([.top, .bottom], 15)
                        .background(.red).brightness(0.13)
                        .cornerRadius(9)
                })
            }
        }
        .navigationTitle(isCreator ? "Tvůrce!" : "Tvůrce?")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct UserTypeView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            UserTypeView(signInViewModel: .init(), profileViewModel: .init(), rootActive: .constant(false), signInUsername: .constant(""), registered: .constant(true))
        }
    }
}

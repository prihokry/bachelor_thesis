//
//  SignInView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 14.03.2022.
//

import SwiftUI

struct SignInView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    @EnvironmentObject var signInViewModel: SignInViewModel
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    
    @State var username = ""
    @State var password = ""
    
    @State var hiddenPassword = true
    
    @State var fillUsernameAlert = false
    @State var fillPasswordAlert = false
    
    @Environment(\.dismiss) private var dismiss
    
    //https://stackoverflow.com/questions/57334455/swiftui-how-to-pop-to-root-view/59662275#59662275
    @State var rootActive = false
    @State var registered = false
    
    var body: some View {
        NavigationView {
            VStack {
                //Logo
                RoundedRectangle(cornerRadius: 9)
                    .frame(width: 150, height: 150)
                    .padding(.bottom, 10)
                    .foregroundColor(.gray).brightness(0.15)
                
                VStack {
                    Text("Přihlášení.")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding(.bottom, 1)
                    
                    Button(action: {}, label: {
                        NavigationLink(destination: RegisterUserView(rootActive: $rootActive, signInUsername: $username, registered: $registered),
                                       isActive: $rootActive,
                                       label: {
                            Text("nebo registrace zde")
                                .foregroundColor(.gray)
                                .font(.system(size: 13))
                                .underline()
                        })
                            .isDetailLink(false)
                    })
                }
                .padding(.bottom, registered ? 10 : 40)
                
                if registered {
                    Text("Pro dokončení registrace klikněte na odkaz zaslaný na Vaši emailovou adresu.")
                        .font(.system(size: 13))
                        .frame(height: 38)
                        .padding()
                        .background(.green).brightness(0.22)
                        .cornerRadius(9)
                }
                
                LoginCredentialsView(username: $username, password: $password,
                                     fillUsernameAlert: $fillUsernameAlert, passwordAlert: $fillPasswordAlert)
                
                if !signInViewModel.authorizingUsername && !signInViewModel.userExists {
                    Text("Uživatelské jméno neexistuje!")
                        .font(.system(size: 14))
                        .foregroundColor(.red).brightness(0.1)
                        .padding(.bottom, 7)
                }
                
                if !signInViewModel.authorizingUsername && signInViewModel.userExists &&
                   !signInViewModel.authorizingPassword && !signInViewModel.passwordCorrect {
                    Text("Nesprávné heslo!")
                        .font(.system(size: 14))
                        .foregroundColor(.red).brightness(0.1)
                        .padding(.bottom, 7)
                }
                
                Button(action: {
                    var faultyData = false
                    if username.isEmpty {
                        fillUsernameAlert = true
                        faultyData = true
                    }
                    
                    if password.isEmpty {
                        fillPasswordAlert = true
                        faultyData = true
                    }
                    
                    if faultyData {
                        faultyData.toggle()
                        return
                    }
                    
                    signInViewModel.userRegistered(usernameInput: username)
                    signInViewModel.isPasswordCorrect(usernameInput: username, passwordInput: password)
                }) {
                    Text("Přihlásit se")
                        .foregroundColor(.white)
                        .padding([.leading, .trailing], 100)
                        .padding([.top, .bottom], 15)
                        .background(.black).brightness(0.13)
                        .cornerRadius(9)
                }
                    .padding(.top, 9)
            }
            .onChange(of: signInViewModel.userExists, perform: { _ in
                if signInViewModel.userExists && signInViewModel.passwordCorrect {
                    profileViewModel.reset()
                    signInViewModel.username = username
                    dismiss()
                }
            })
            .onChange(of: signInViewModel.passwordCorrect, perform: { _ in
                if signInViewModel.passwordCorrect && signInViewModel.userExists {
                    profileViewModel.reset()
                    signInViewModel.username = username
                    dismiss()
                }
            })
            .padding([.leading, .trailing], 25)
            .navigationBarHidden(true)
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView(registered: false)
            .environmentObject(ProfileViewModel.init())
            .environmentObject(SignInViewModel.init())
            .environmentObject(MessagesViewModel.init())
    }
}

//
//  RegisterCreator.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 15.03.2022.
//

import SwiftUI

struct RegisterCreator: View {
    @EnvironmentObject var signInViewModel: SignInViewModel
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @Binding var rootActive: Bool
    
    @Binding var registered: Bool
    
    @Binding var signInUsername: String
    
    @State var yearsOfExperience = ""
    @State var profileText = ""
    
    @State var yoeAlert = false
    @State var profileTextAlert = false
    
    var body: some View {
        VStack {
            HStack {
                Text("Počet let zkušeností")
                    .font(.system(size: 16))
                    .foregroundColor(.black).brightness(0.25)
                
                ZStack(alignment: .leading) {
                        
                    TextField("3", text: $yearsOfExperience)
                        .frame(width: 50, height: 38)
                        .offset(x: 17)
                        .background(.gray).brightness(0.35)
                        .font(.system(size: 16))
                        .cornerRadius(9)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .keyboardType(.decimalPad)
                        .overlay(
                            RoundedRectangle(cornerRadius: 9)
                                .stroke(.red, lineWidth: 2)
                                .opacity(yoeAlert ? 1 : 0)
                        )
                }
                
                Spacer()
            }
            
            VStack {
                HStack {
                    Text("Profilový popis")
                        .font(.system(size: 16))
                        .foregroundColor(.black).brightness(0.25)
                    
                    Spacer()
                }
                
                        
                TextEditor(text: $profileText)
                    .lineSpacing(1)
                    .frame(height: 85)
                    .font(.system(size: 16))
                    .padding(.leading, 13)
                    .padding(.top, 9)
                    .background(.gray).brightness(0.35)
                    .cornerRadius(9)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .overlay(
                        RoundedRectangle(cornerRadius: 9)
                            .stroke(.red, lineWidth: 2)
                            .opacity(profileTextAlert ? 1 : 0)
                    )
                
                Spacer()
            }
            
            Button(action: {
                var faultyData = false
                
                if yearsOfExperience.isEmpty {
                    yoeAlert = true
                    faultyData = true
                }
                
                if let yoe = Int(yearsOfExperience) {
                    if yoe < 0 {
                        yoeAlert = true
                        faultyData = true
                    }
                } else {
                    yoeAlert = true
                    faultyData = true
                }
                
                if profileText.isEmpty {
                    profileTextAlert = true
                    faultyData = true
                }
                
                if faultyData { return }
                
                profileViewModel.user!.creator = Creator(profileText: profileText, yearsOfExperience: Int(yearsOfExperience)!)
                
                let _ = signInViewModel.userViewModel.users.popLast()
                let _ = profileViewModel.userViewModel.users.popLast()
                
                signInViewModel.userViewModel.users.append(profileViewModel.user!)
                profileViewModel.userViewModel.users.append(profileViewModel.user!)
                
                signInUsername = profileViewModel.user!.username
                registered = true
                rootActive = false
            }, label: {
                Text("Registrovat")
                    .foregroundColor(.white)
                    .padding([.leading, .trailing], 100)
                    .padding([.top, .bottom], 15)
                    .background(.red).brightness(0.13)
                    .cornerRadius(9)
            })
        }
        .padding()
        .navigationTitle("Registrace tvůrce")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            UITextView.appearance().backgroundColor = .clear
        }
    }
}

struct RegisterCreator_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RegisterCreator(signInViewModel: .init(), profileViewModel: .init(), rootActive: .constant(false), registered: .constant(true), signInUsername: .constant(""))
        }
    }
}

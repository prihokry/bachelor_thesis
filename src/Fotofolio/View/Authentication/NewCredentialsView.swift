//
//  NewUserCredentialsView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 19.03.2022.
//

import SwiftUI

struct NewCredentialsView: View {
    @Binding var username: String
    @Binding var password: String
    @Binding var passwordSecond: String
    
    @State var hiddenFirstPassword = true
    @State var hiddenSecondPassword = true
    
    @Binding var usernameAlert: Bool
    @Binding var userNameExists: Bool
    @Binding var badUsernameCharacters: Bool
    
    var body: some View {
        VStack {
            Text("Uživatelské jméno")
                .brightness(0.27)
            
            TextField("Uživatelské jméno", text: $username)
                .font(.system(size: 18))
                .frame(height: 38)
                .offset(x: 9)
                .padding()
                .background(.gray).brightness(0.35)
                .cornerRadius(9)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .overlay(
                    RoundedRectangle(cornerRadius: 9)
                        .stroke(.red, lineWidth: 1)
                        .opacity(usernameAlert ? 1 : 0)
                )
            
            if userNameExists {
                Text("Tohle uživatelské jméno už existuje... Zvolte jiné!")
                    .font(.system(size: 14))
                    .foregroundColor(.red).brightness(0.1)
                    .padding(.bottom, 7)
            }
            
            if badUsernameCharacters {
                Text("Podporované znaky v uživatelském jméně: a-z 0-9 _")
                    .font(.system(size: 14))
                    .foregroundColor(.red).brightness(0.1)
                    .padding(.bottom, 7)
            }
            
            Text("Heslo")
                .brightness(0.27)
            
            ZStack(alignment: .trailing) {
                if hiddenFirstPassword {
                    SecureField("Heslo", text: $password)
                        .textContentType(.oneTimeCode)
                        .font(.system(size: 18))
                        .frame(height: 38)
                        .offset(x: 9)
                        .padding()
                        .background(.gray).brightness(0.35)
                        .cornerRadius(9)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                } else {
                    TextField("Heslo", text: $password)
                        .font(.system(size: 18))
                        .frame(height: 38)
                        .offset(x: 9)
                        .padding()
                        .background(.gray).brightness(0.35)
                        .cornerRadius(9)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                }
                
                Button(action: { withAnimation { hiddenFirstPassword.toggle() } }, label: {
                    Image(systemName: hiddenFirstPassword ? "eye" : "eye.slash")
                        .padding(.trailing, 17)
                        .foregroundColor(.gray)
                })
            }
            
            Text("Potvrzení hesla")
                .brightness(0.27)
            
            HStack {
                ZStack(alignment: .trailing) {
                    if hiddenSecondPassword {
                        SecureField("Potvrzení hesla", text: $passwordSecond)
                            .textContentType(.oneTimeCode)
                            .font(.system(size: 18))
                            .frame(height: 38)
                            .offset(x: 9)
                            .padding()
                            .background(.gray).brightness(0.35)
                            .cornerRadius(9)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                    } else {
                        TextField("Potvrzení hesla", text: $passwordSecond)
                            .font(.system(size: 18))
                            .frame(height: 38)
                            .offset(x: 9)
                            .padding()
                            .background(.gray).brightness(0.35)
                            .cornerRadius(9)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                    }
                    
                    Button(action: { withAnimation { hiddenSecondPassword.toggle() } }, label: {
                        Image(systemName: hiddenSecondPassword ? "eye" : "eye.slash")
                            .padding(.trailing, 17)
                            .foregroundColor(.gray)
                    })
                }
                
                Image(systemName: (!password.isEmpty && !passwordSecond.isEmpty && password == passwordSecond) ? "checkmark.circle.fill" : "xmark.circle.fill")
                    .foregroundColor((!password.isEmpty && !passwordSecond.isEmpty && password == passwordSecond) ? .green : .red)
            }
            
            if password.isEmpty || passwordSecond.isEmpty || password != passwordSecond {
                Text("Hesla se neshodují (nebo jsou pole prázdná)!")
                    .font(.system(size: 14))
                    .foregroundColor(.red).brightness(0.1)
                    .padding(.bottom, 7)
            }
        }
    }
}

struct NewUserCredentialsView_Previews: PreviewProvider {
    static var previews: some View {
        NewCredentialsView(username: .constant("user"), password: .constant("pswd"), passwordSecond: .constant("pswd"),
                           usernameAlert: .constant(false), userNameExists: .constant(false), badUsernameCharacters: .constant(false))
    }
}

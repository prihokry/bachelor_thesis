//
//  ChatView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 20.03.2022.
//

import SwiftUI

struct ChatView: View {
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    @EnvironmentObject var userViewModel: UserViewModel
    
    @AppStorage("username") var sender: String = "default"
    let receiver: User
    
    @State var newMessage = ""
    
    var body: some View {
        VStack {
            let chatIndex = messagesViewModel.chats.firstIndex(where: { chat in
                chat.chatOwners.contains(where: { $0.username == receiver.username })
            })
            
            ScrollView {
                VStack {
                    if let chatIndex = chatIndex {
                        ForEach(messagesViewModel.chats[chatIndex].messages) { message in
                            HStack {
                                let isSender = message.from == sender
                                
                                if isSender { Spacer() }
                                
                                Text(message.body)
                                    .font(.system(size: 15))
                                    .multilineTextAlignment(.leading)
                                    .padding(15)
                                    .background(isSender ? .red.opacity(0.13) : .gray.opacity(0.2))
                                    .cornerRadius(9)

                                if !isSender { Spacer() }
                            }
                        }
                    } else {
                        Text("Žádné zprávy.")
                            .foregroundColor(.gray)
                    }
                }
            }
            
            HStack {
                ZStack {
                    Rectangle()
                       .foregroundColor(.gray).brightness(0.37)
                    
                    TextField("...", text: $newMessage)
                        .autocapitalization(.none)
                        .padding()
                 }
                .frame(height: 45)
                .cornerRadius(10)
                
                Button(action: {
                    let message = Message(from: sender, to: receiver.username, body: newMessage, timestamp: .now)
                    
                    if let chatIndex = chatIndex {
                        messagesViewModel.chats[chatIndex].messages.append(message)
                    } else {
                        messagesViewModel.chats.append(Chat(chatOwners: [userViewModel.getUserByUsername(sender)!, receiver], messages: [message]))
                    }
                    
                    newMessage = ""
                }, label: {
                    Text("Odeslat")
                        .padding([.top, .bottom], 11)
                        .padding([.leading, .trailing], 7)
                        .background(.red).brightness(0.35)
                        .foregroundColor(.white)
                        .cornerRadius(9)
                })
            }
        }
        .padding()
        .navigationTitle("@\(receiver.username)")
        .navigationBarTitleDisplayMode(.inline)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ChatView(messagesViewModel: .init(), receiver: .dummy2)
        }
    }
}

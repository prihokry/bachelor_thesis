//
//  MessagesView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 30.01.2022.
//

import SwiftUI

struct MessagesView: View {
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    
    @State var messageDirectlyTo: User?
    
    @State var directMessagePossible: Bool = false
    
    @AppStorage("username") var username: String = "default"
    
    var body: some View {
        GeometryReader { geo in
            ScrollView {
                if messagesViewModel.fetchingChats {
                    ProgressView()
                        .frame(width: geo.size.width, height: geo.size.height)
                } else {
                    if messagesViewModel.chats.isEmpty {
                        Text("Žádné zprávy.")
                            .frame(width: geo.size.width, height: geo.size.height)
                            .font(.system(size: 15))
                            .foregroundColor(.gray)
                    } else {
                        ForEach(messagesViewModel.chats) { chat in
                            let optReceiver = chat.chatOwners.filter({
                                $0.username != username
                            }).first
                            
                            if let receiver = optReceiver {
                                Button(action: { }) {
                                    NavigationLink(destination: ChatView(receiver: receiver), label: {
                                        HStack {
                                            if let pic = receiver.profilePicture {
                                                if case MyImageEnum.remote(let url) = pic.src {
                                                    AsyncImage(url: URL(string: url)!) { image in
                                                        image
                                                            .resizable()
                                                            .aspectRatio(1.0, contentMode: .fit)
                                                            .frame(width: 60, height: 60)
                                                            .cornerRadius(100)
                                                    } placeholder: {
                                                        ZStack {
                                                            Circle()
                                                                .foregroundColor(.gray).brightness(0.33)
                                                                .frame(width: 60, height: 60)

                                                            ProgressView()
                                                                .progressViewStyle(CircularProgressViewStyle())
                                                        }
                                                    }
                                                } else if case MyImageEnum.local(let img) = pic.src {
                                                    img
                                                        .resizable()
                                                        .aspectRatio(1.0, contentMode: .fit)
                                                        .frame(width: 60, height: 60)
                                                        .cornerRadius(100)
                                                }
                                            } else {
                                                Circle()
                                                    .foregroundColor(.gray).brightness(0.33)
                                                    .frame(width: 60, height: 60)
                                            }
                                            
                                            VStack(alignment: .leading, spacing: 3) {
                                                Text("@" + receiver.username)
                                                    .font(.system(size: 17))
                                                    .fontWeight(.medium)
                                                    .foregroundColor(.red).brightness(0.1)
                                                
                                                if let message = chat.messages.last {
                                                    Text(message.from == username ? "Vy: \"\(message.body)\"" : "\"\(message.body)\"")
                                                        .foregroundColor(.gray)
                                                        .font(.system(size: 14))
                                                } else {
                                                    Text("\"Poslední zpráva\"")
                                                        .foregroundColor(.gray)
                                                        .font(.system(size: 15))
                                                }
                                            }
                                            .padding(.leading, 10)
                                            
                                            Spacer()
                                        }
                                        .padding([.top, .bottom], 10)
                                    })
                                }
                            }
                            
                            if messagesViewModel.chats.last!.id != chat.id {
                                Divider()
                            }
                        }
                    }
                }
            }
        }
        .padding()
        .onAppear {
            if messagesViewModel.chats.isEmpty {
                messagesViewModel.fetchChats(for: username)
            }
        }
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: { }) {
                    NavigationLink(destination: NewChatSearchView()) {
                        Image(systemName: "square.and.pencil")
                            .resizable()
                            .aspectRatio(1, contentMode: .fit)
                            .frame(height: 15)
                            .foregroundColor(.gray)
                    }
                }
                .padding(.trailing, 5)
            }
        }
        .navigationTitle("Zprávy")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct MessagesView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MessagesView()
                .environmentObject(MessagesViewModel.init())
        }
    }
}

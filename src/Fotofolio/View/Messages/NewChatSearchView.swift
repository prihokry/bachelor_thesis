//
//  NewChatView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 20.03.2022.
//

import SwiftUI

struct NewChatSearchView: View {
    @EnvironmentObject var messagesViewModel: MessagesViewModel
    @StateObject var searchViewModel: SearchViewModel = .init()
    
    @AppStorage("username") var signedUser: String = "default"
    
    var body: some View {
        ScrollView {
            VStack {
                ZStack {
                    Rectangle()
                       .foregroundColor(.gray).brightness(0.37)
                    
                    HStack {
                        Image(systemName: "magnifyingglass")
                        TextField("Hledat", text: $searchViewModel.searchString) { editing in
                            if editing {
                                searchViewModel.searching = true
                            }
                        } onCommit: {
                            searchViewModel.searching = false
                        }
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                    }
                    .padding()
                 }
                .frame(height: 45)
                .cornerRadius(10)
                .padding([.leading, .trailing])
                
                //results
                ForEach(searchViewModel.searchedUsers) { user in
                    if user.username != signedUser {
                        NavigationLink(destination: ChatView(receiver: user)) {
                            HStack {
                                ProfilePicView(profilePicture: user.profilePicture, resolution: 60.0)
                                    .padding(.leading, 20)
                                
                                VStack(alignment: .leading, spacing: 0) {
                                    Text(user.username)
                                        .font(.system(size: 16))
                                        .foregroundColor(.black).brightness(0.2)
                                        .padding(.bottom, 4)
                                    
                                    Text(user.location)
                                        .font(.system(size: 13))
                                        .foregroundColor(.black).brightness(0.3)
                                        .padding(.bottom, 2)
                                }
                                .padding(.leading, 7)
                                
                                Spacer()
                            }
                            .padding(.top, 12)
                            .transition(.move(edge: .trailing))
                        }
                    }
                }
            }
        }
        .navigationTitle("Nový chat")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            if searchViewModel.searching {
                Button("Zrušit") {
                    searchViewModel.searchString = ""
                    searchViewModel.searching = false
                    
                    UIApplication.shared.dismissKeyboard()
                }
                .transition(.opacity)
            }
        }
        .onChange(of: searchViewModel.searchString, perform: { _ in
            withAnimation {
                searchViewModel.searchedUsers = searchViewModel.fetchUsersBySubstring(input: searchViewModel.searchString, mode: searchViewModel.searchMode)
            }
        })
        .gesture(DragGesture()
                    .onChanged({ _ in
            UIApplication.shared.dismissKeyboard()
            
            withAnimation {
                searchViewModel.searching = false
            }
        })
        )
    }
}

struct NewChatSearchView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            NewChatSearchView(messagesViewModel: .init())
        }
    }
}

//
//  EditPersonalDetailsView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 01.03.2022.
//

import SwiftUI

struct EditPersonalDetailsView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @State private var newProfileImage: UIImage?
    @State private var isPickerPresented = false
    
    @Binding var dataChanged: Bool
    
    @Binding var location: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                ZStack {
                    ProfilePicView(profilePicture: profileViewModel.user?.profilePicture, resolution: 80.0, hideProgress: true)
                    
                    Button(action: { isPickerPresented = true }) {
                        ZStack {
                            Circle()
                                .fill(.gray)
                                .frame(width: 40, height: 40)
                                .opacity(0.75)
                            
                            Image(systemName: "pencil")
                                .foregroundColor(.gray)
                                .brightness(0.37)
                        }
                    }
                }
                .padding(.leading, 20)
                
                VStack(alignment: .leading) {
                    Text("@" + (profileViewModel.user?.username ?? "Uživatelské jméno se nepodařilo načíst..."))
                        .font(.system(size: 18))
                        .foregroundColor(.red).brightness(0.2)
                        .padding(.leading, 20)
                    
                    Text(profileViewModel.user?.fullName ?? "Jméno se nepodařilo načíst...")
                        .font(.system(size: 18))
                        .foregroundColor(.black).brightness(0.2)
                        .padding(.bottom, 4)
                        .padding(.leading, 20)
                }
                
                Spacer()
            }
            .padding(.top, 10)
            .padding(.bottom, 10)
            
            VStack(alignment: .leading) {
                Text("Místo působení:")
                    .font(.system(size: 16))
                    .foregroundColor(.black).brightness(0.25)
                    .padding(.leading, 20)
                    .padding(.bottom, -1)
                
                ZStack {
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray).brightness(0.42)
                        .frame(height: 45)
                        .offset(x: -5)
                    
                    TextEditor(text: $location)
                        .lineSpacing(1)
                        .frame(height: 38)
                        .font(.system(size: 16))
                        .foregroundColor(.gray)
                        .onTapGesture {
                            dataChanged = true
                        }
                }
                .padding(.leading, 16)
                
                Spacer()
            }
        }
        .onAppear {
            location = profileViewModel.user?.location ?? ""
        }
        .onChange(of: location, perform: { _ in
            profileViewModel.user?.location = location
        })
        .sheet(isPresented: $isPickerPresented, onDismiss: {
            guard let newProfileImage = newProfileImage else { return }

            profileViewModel.user?.profilePicture = IImage(src: MyImageEnum.local(Image(uiImage: newProfileImage)))
            self.newProfileImage = nil
        }, content: {
            ImagePicker(isPresented: $isPickerPresented, image: $newProfileImage)
        })
    }
}

struct EditPersonalDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        EditPersonalDetailsView(profileViewModel: .init(), dataChanged: .constant(false), location: .constant("Praha"))
    }
}

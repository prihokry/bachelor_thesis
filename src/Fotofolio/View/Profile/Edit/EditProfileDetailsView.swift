//
//  EditProfileDetailsView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 01.03.2022.
//

import SwiftUI

struct EditProfileDetailsView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @Binding var dataChanged: Bool
    
    @Binding var yearsOfExperience: String
    @Binding var profileText: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack {
                Text("Počet let zkušeností:")
                    .font(.system(size: 16))
                    .foregroundColor(.black).brightness(0.25)
                    .padding(.top, 5)
                    .padding(.bottom, 8)
                
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray).brightness(0.42)
                        .frame(width: 50, height: 40)
                        
                    TextField("3", text: $yearsOfExperience, onEditingChanged: { _ in dataChanged = true })
                        .offset(x: 17)
                        .foregroundColor(.gray)
                        .font(.system(size: 16))
                        .keyboardType(.decimalPad)
                }
                
                Spacer()
            }
            .padding(.leading, 20)
            
            VStack {
                HStack {
                    Text("Profilový popis:")
                        .font(.system(size: 16))
                        .foregroundColor(.black).brightness(0.25)
                    
                    Spacer()
                }
                .padding(.bottom, -7)
                .padding(.leading, 20)
                
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray).brightness(0.42)
                        .frame(height: 85)
                        .padding(.trailing, 7)
                        
                    TextEditor(text: $profileText)
                        .lineSpacing(1)
                        .frame(width: 340, height: 85)
                        .font(.system(size: 16))
                        .padding(.top, 6)
                        .foregroundColor(.gray)
                        .offset(x: 7)
                        .onTapGesture {
                            dataChanged = true
                        }
                }
                .padding(.leading, 16)
                
                Spacer()
            }
            
            Divider()
                .padding(.bottom, 2)
                .padding(.top, 5)
                .padding([.leading, .trailing], 7)
        }
        .onAppear {
            let number = profileViewModel.user?.creator?.yearsOfExperience ?? 1
            yearsOfExperience = String(number)
            
            profileText = profileViewModel.user?.creator?.profileText ?? ""
        }
        .onChange(of: profileText, perform: { _ in
            profileViewModel.user?.creator?.profileText = profileText
        })
        .onChange(of: yearsOfExperience) { _ in
            if let yoe = Int(yearsOfExperience) {
                profileViewModel.user?.creator?.yearsOfExperience = yoe
            }
        }
    }
}

struct EditProfileDetailsView_Previews: PreviewProvider {
    static var previews: some View {
//        EditProfileDetailsView(user: .constant(.dummy1))
        EditProfileDetailsView(profileViewModel: .init(), dataChanged: .constant(false),
                               yearsOfExperience: .constant("1"), profileText: .constant("Profile text"))
    }
}

//
//  EditPortfolioView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 01.03.2022.
//

import SwiftUI

struct EditPortfolioView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @Binding var dataChanged: Bool
    
    @State private var newPortfolioImage: UIImage?
    @State private var isPickerPresented = false
    
    @State private var tagInput = ""
    @State private var addToPfolioIndex: Int?
    @State private var removePortfolioConfirm = false
    @State private var removePfolioIndex: Int = 0
    
    var body: some View {
        ForEach(Array(profileViewModel.portfolios.enumerated()), id: \.1.id) { pfolioIndex, portfolio in
            VStack(alignment: .leading) {
                HStack {
                    Button(action: {
                        removePfolioIndex = pfolioIndex
                        dataChanged = true
                        removePortfolioConfirm = true
                    }) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 9)
                                .fill()
                                .foregroundColor(.red).brightness(0.42)
                                .frame(width: 45, height: 38)
                                .opacity(0.25)
                            
                            Image(systemName: "trash")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 20, height: 20)
                                .foregroundColor(.red)
                        }
                    }
                    .confirmationDialog("Opravdu chcete portfolio odstranit?", isPresented: $removePortfolioConfirm) {
                        Button("Odstranit", role: .destructive) {
                            dataChanged = true
                            
                            withAnimation {
                                let _ = profileViewModel.portfolios.remove(at: removePfolioIndex)
                            }
                        }
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 7)
                            .foregroundColor(.gray).brightness(0.4)
                            .frame(height: 38)
                        
                        TextField("např. Portréty", text: $profileViewModel.portfolios[pfolioIndex].name)
                            .font(.system(size: 18))
                            .frame(height: 38)
                            .foregroundColor(.gray)
                            .offset(x: 9)
                            .onTapGesture {
                                dataChanged = true
                            }
                    }
                    
                    Spacer()
                }
                .padding(.top, 6)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHStack {
                        Button(action: {
                            dataChanged = true
                            isPickerPresented = true;
                            addToPfolioIndex = profileViewModel.portfolios.firstIndex(of: portfolio)
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 9)
                                    .fill(Color.gray).brightness(0.34)
                                    .aspectRatio(1.0, contentMode: .fit)
                                    .frame(width: 90, height: 90)
                                
                                Image(systemName: "plus")
                                    .resizable()
                                    .frame(width: 17, height: 17)
                                    .foregroundColor(.black).brightness(0.3)
                            }
                        }
                        
                        ForEach(portfolio.photos, id: \.id) { img in
                            ZStack {
                                if case MyImageEnum.remote(let url) = img.src {
                                    AsyncImage(url: URL(string: url)!) { image in
                                        ZStack {
                                            image
                                                .resizable()
                                                .aspectRatio(1.0, contentMode: .fill)
                                                .frame(width: 90, height: 90)
                                                .cornerRadius(CGFloat(9))
                                                .blur(radius: 1.5)
                                                .clipShape(RoundedRectangle(cornerRadius: 9))
                                            
                                            RoundedRectangle(cornerRadius: 9)
                                                .fill(Color.gray)
                                                .aspectRatio(1.0, contentMode: .fit)
                                                .frame(width: 90, height: 90)
                                                .opacity(0.65)
                                        }
                                    } placeholder: {
                                        RoundedRectangle(cornerRadius: 9)
                                            .fill(Color.gray).brightness(0.25)
                                            .aspectRatio(1.0, contentMode: .fit)
                                            .frame(width: 90, height: 90)
                                    }
                                } else if case MyImageEnum.local(let image) = img.src {
                                    ZStack {
                                        image
                                            .resizable()
                                            .aspectRatio(1.0, contentMode: .fill)
                                            .frame(width: 90, height: 90)
                                            .cornerRadius(CGFloat(9))
                                            .blur(radius: 1.5)
                                            .clipShape(RoundedRectangle(cornerRadius: 9))
                                        
                                        RoundedRectangle(cornerRadius: 9)
                                            .fill(Color.gray)
                                            .aspectRatio(1.0, contentMode: .fit)
                                            .frame(width: 90, height: 90)
                                            .opacity(0.65)
                                    }
                                }
                                
                                Button(action: {
                                    dataChanged = true
                                    
                                    let portIndex = profileViewModel.portfolios.firstIndex(of: portfolio)
                                    guard let portIndex = portIndex else { return }
                                    
                                    let photoIndex = profileViewModel.portfolios[portIndex].photos.firstIndex(where: { $0.id == img.id })
                                    guard let photoIndex = photoIndex else { return }
                                    
                                    withAnimation {
                                        let _ = profileViewModel.portfolios[portIndex].photos.remove(at: photoIndex)
                                    }
                                }) {
                                    Image(systemName: "xmark")
                                        .resizable()
                                        .frame(width: 17, height: 17)
                                        .foregroundColor(.red)
                                        .opacity(0.9)
                                        .padding()
                                }
                            }
                        }
                        .transition(.opacity)
                    }
                }
                
                ZStack {
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray).brightness(0.42)
                        .frame(height: 85)
                        .padding(.trailing, 7)
                    
                    TextEditor(text: $profileViewModel.portfolios[pfolioIndex].description)
                        .font(.system(size: 16))
                        .frame(height: 85)
                        .lineSpacing(2)
                        .foregroundColor(.gray)
                        .padding([.leading, .top], 5)
                        .onTapGesture {
                            dataChanged = true
                        }
                }
                
                if portfolio.tags.count < 5 {
                    HStack {
                        ZStack {
                            Rectangle()
                               .foregroundColor(.gray).brightness(0.37)
                            
                            TextField("např. svatba", text: $tagInput)
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .padding()
                         }
                        .frame(height: 40)
                        .cornerRadius(7)
                        
                        Button(action: {
                            addTag(pIndex: pfolioIndex)
                        }, label: {
                            Text("Přidat")
                                .padding(10)
                                .background(.red).brightness(0.5)
                                .foregroundColor(.white)
                                .cornerRadius(7)
                        })
                            .padding(.trailing, 7)
                    }
                    .transition(.opacity)
                }
                
                VStack(alignment: .leading) {
                    ForEach(Array(portfolio.tags.enumerated()), id: \.1.self) { i, tag in
                        HStack {
                            Text(tag)
                                .padding([.leading, .trailing], 9)
                                .padding([.bottom, .top], 7)
                                .background(.gray).brightness(0.4)
                                .cornerRadius(7)
                            
                            Button(action: {
                                withAnimation {
                                    let _ = profileViewModel.portfolios[pfolioIndex].tags.remove(at: i)
                                }
                            }) {
                                Image(systemName: "xmark")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(width: 12, height: 12)
                                    .foregroundColor(.gray).brightness(0.15)
                            }
                        }
                    }
                    .transition(.opacity)
                }
            }
            .padding(.leading)
        }
        .sheet(isPresented: $isPickerPresented, onDismiss: {
            guard let portfolioIndexAdd = addToPfolioIndex else { return }
            guard let newPortfolioImage = newPortfolioImage else { return }

            profileViewModel.portfolios[portfolioIndexAdd].photos.append(IImage(src: .local(Image(uiImage: newPortfolioImage))))
            self.newPortfolioImage = nil
        }, content: {
            ImagePicker(isPresented: $isPickerPresented, image: $newPortfolioImage)
        })
        .navigationBarBackButtonHidden(true)
    }
    
    private func addTag(pIndex: Int) {
        if !profileViewModel.portfolios[pIndex].tags.contains(tagInput)
            && profileViewModel.portfolios[pIndex].tags.count < 5 {
            withAnimation {
                profileViewModel.portfolios[pIndex].tags.append(tagInput.replacingOccurrences(of: " ", with: "").lowercased())
                tagInput = ""
            }
        }
    }
}

struct EditPortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        EditPortfolioView(dataChanged: .constant(false))
            .environmentObject(ProfileViewModel.init())
    }
}

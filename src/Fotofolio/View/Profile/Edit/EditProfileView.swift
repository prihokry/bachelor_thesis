//
//  EditProfileView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 01.03.2022.
//

import SwiftUI

struct EditProfileView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @AppStorage("username") var username: String = "default"
    @Environment(\.dismiss) private var dismiss
    
    @State var dataChanged = false
    @State var cancelAlertShowing = false
    @State var returnToProfile = false
    
    //variables for reverting changes
    @State var portfoliosInitial: [Portfolio] = []
    @State var userInitial: User?
    
    //tmp variables for user profile
    @State var yearsOfExperience: String = "0"
    @State var profileText: String = ""
    @State var location: String = ""
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                //Profile info
                EditPersonalDetailsView(dataChanged: $dataChanged, location: $location)
                
                if profileViewModel.user?.creator != nil {
                    EditProfileDetailsView(dataChanged: $dataChanged, yearsOfExperience: $yearsOfExperience, profileText: $profileText)
                    EditPortfolioView(dataChanged: $dataChanged)
                }
            }
        }
        .navigationTitle("Úprava profilu")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .onChange(of: returnToProfile, perform: { _ in dismiss() })
        .onAppear {
            UITextView.appearance().backgroundColor = .clear
            
            //backing up the state in the beginning
            if let user = profileViewModel.user {
                userInitial = user
            }
            
            portfoliosInitial = profileViewModel.portfolios
        }
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: {
                    //years of experience!
                    
                    profileViewModel.user?.location = location
                    profileViewModel.user?.creator?.profileText = profileText
                    
                    dismiss()
                }) {
                    Text("Uložit")
                        .foregroundColor(.gray)
                }
                .padding(.trailing, 5)
            }
            
            ToolbarItemGroup(placement: .navigationBarLeading) {
                Button(action: {
                    if dataChanged {
                        cancelAlertShowing = true
                        return
                    }
                    
                    dismiss()
                }) {
                    Text("Zrušit")
                        .foregroundColor(.gray)
                }
                .padding(.leading, 5)
            }
        }
        .alert("Opravdu chcete zahodit provedené změny?", isPresented: $cancelAlertShowing) {
            Button("Ne") { cancelAlertShowing = false }
            
            Button("Ano") {
                //reset profile info
                profileViewModel.portfolios = portfoliosInitial
                profileViewModel.user = userInitial
                
                returnToProfile = true
                cancelAlertShowing = false
            }
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            EditProfileView(profileViewModel: .init())
        }
    }
}

//
//  ProfileInfoView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 09.02.2022.
//

import SwiftUI

struct ProfileInfoView: View {
    @Binding var user: User?
    @State var profileOwner: Bool
    
    @State var showRating = false
    @State var tmpRatings: [String:Int] = [:]
    @State var assigned = false
    
    var body: some View {
        if let user = user {
            VStack {
                HStack {
                    ProfilePicView(profilePicture: user.profilePicture, resolution: 80.0)
                        .padding(.leading, 20)
                    
                    VStack(alignment: .leading, spacing: 0) {
                        Text(user.fullName)
                            .font(.system(size: 17))
                            .foregroundColor(.black).brightness(0.2)
                            .padding(.bottom, 4)
                        
                        Text(user.location)
                            .font(.system(size: 13))
                            .foregroundColor(.black).brightness(0.3)
                            .padding(.bottom, 2)
                        
                        if !user.ratings.isEmpty {
                            HStack {
                                Text(String(format: "%.1f", user.calculateRating()) + " z 5")
                                    .font(.system(size: 12))
                                    .foregroundColor(.black).brightness(0.3)
                                
                                Image(systemName: "star.fill")
                                    .font(.system(size: 8))
                                    .foregroundColor(.black).brightness(0.3)
                                    .offset(x: -5)
                            }
                        } else {
                            Text("Zatím bez hodnocení.")
                                .font(.system(size: 12))
                                .foregroundColor(.black).brightness(0.3)
                        }
                        
                        if !profileOwner {
                            Button(action: { }, label: {
                                NavigationLink(destination: RatingView(ratings: $tmpRatings), label: {
                                    Text("Udělit hodnocení")
                                        .font(.system(size: 14))
                                        .underline()
                                        .foregroundColor(.red).brightness(0.25)
                                })
                            })
                        }
                    }
                    .padding(.leading, 20)
                    
                    Spacer()
                }
                .padding(.top, 10)
                
                Divider()
                    .padding(.leading, 18)
                    .padding(.trailing, 18)
                    .padding(.bottom, 2)
                    .padding(.top, 5)
                
                if let creator = user.creator {
                    VStack(alignment: .leading, spacing: 0) {
                        HStack {
                            Text("\(creator.yearsOfExperience)" + yearFormatString(creator.yearsOfExperience))
                                .font(.system(size: 15))
                                .padding(.top, 5)
                                .padding(.leading, 23)
                                .foregroundColor(.black).brightness(0.3)
                            
                            Spacer()
                        }
                        
                        Text(creator.profileText)
                            .lineSpacing(1)
                            .font(.system(size: 15))
                            .padding(.top, 6)
                            .padding(.leading, 23)
                            .padding(.trailing, 5)
                            .foregroundColor(.gray).brightness(0.1)
                    }
                    
                    Divider()
                        .padding(.leading, 18)
                        .padding(.trailing, 18)
                        .padding(.bottom, 10)
                        .padding(.top, 5)
                }
            }
            .onAppear(perform: {
                if !assigned {
                    tmpRatings = user.ratings
                    assigned.toggle()
                }
            })
            .onChange(of: tmpRatings, perform: { _ in
                self.user?.ratings = tmpRatings
            })
        } else {
            ProgressView()
                .padding()
        }
    }
    
    func yearFormatString(_ yearsOfExperience: Int) -> String {
        if yearsOfExperience == 1 { return " rok fotografem" }
        if yearsOfExperience < 5 { return " roky fotografem" }
        return " let fotografem"
    }
}

struct ProfileInfoView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileInfoView(user: .constant(User.dummy1), profileOwner: false)
    }
}

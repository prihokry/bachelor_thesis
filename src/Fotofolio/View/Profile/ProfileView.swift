//
//  ProfileView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 30.01.2022.
//

import SwiftUI

struct ProfileView: View {
    @ObservedObject var profileViewModel: ProfileViewModel

    @AppStorage("username") var username: String = "default"
    @State var editProfileBool = false
    
    @Environment(\.dismiss) private var dismiss
    
    @State var user: String
    var profileOwner: Bool = false
    
    @Binding var signOutClear: Bool
    
    var body: some View {
        GeometryReader { geo in
            ScrollView(showsIndicators: false) {
                VStack {
                    if profileViewModel.fetchingInfo {
                        ProgressView()
                            .padding()
                            .frame(width: geo.size.width, height: geo.size.height)
                    } else {
                        ProfileInfoView(user: $profileViewModel.user, profileOwner: profileOwner)
                        
                        if !profileViewModel.fetchingPortfolios {
                            if profileViewModel.user?.creator != nil {
                                ProfilePortfoliosView(profileViewModel: profileViewModel)
                            } else {
                                NotCreatorView()
                            }
                        } else {
                            ProgressView()
                                .padding()
                                .frame(width: geo.size.width, height: geo.size.height)
                        }
                    }
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .principal) {
                Text("@\(user)")
                    .font(.headline)
                    .foregroundColor(.pink)
                    .minimumScaleFactor(0.1)
            }
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                if profileOwner && profileViewModel.user?.creator != nil {
                    Button(action: { }) {
                        NavigationLink(destination: NewPortfolioView()) {
                            Image(systemName: "plus")
                                .resizable()
                                .aspectRatio(1, contentMode: .fit)
                                .frame(height: 15)
                                .foregroundColor(.gray)
                        }
                    }
                }
                
                Button(action: { }) {
                    if profileOwner {
                        NavigationLink(isActive: $editProfileBool, destination: { EditProfileView() }, label: { EmptyView() })
                        
                        Menu(content: {
                            Button(action: { editProfileBool = true }) {
                                Text("Upravit profil")
                                    .foregroundColor(.gray)
                            }
                            
                            Button(action: { signOutClear = true }, label: {
                                Text("Odhlásit se")
                            })
                        }, label: {
                            Image(systemName: "slider.horizontal.3")
                                .resizable()
                                .aspectRatio(1, contentMode: .fit)
                                .frame(height: 15)
                                .foregroundColor(.gray)
                        })
                    } else {
                        if let user = profileViewModel.user {
                            NavigationLink(destination: ChatView(receiver: user)) {
                                Image(systemName: "paperplane")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(height: 18)
                                    .foregroundColor(.gray)
                            }
                        }
                    }
                }
                .padding(.leading, -15)
                .padding(.trailing, 5)
            }
        }
        .onAppear {
            if profileOwner { user = username }
            
            if profileViewModel.user == nil { profileViewModel.fetchProfileInfo(username: user) }
            if profileViewModel.portfolios.isEmpty { profileViewModel.fetchProfilePortofolios(username: user) }
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ProfileView(profileViewModel: .init(), user: "vojtafoti", profileOwner: false, signOutClear: .constant(false))
        }
    }
}

//
//  ProfilePicView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 21.03.2022.
//

import SwiftUI

struct ProfilePicView: View {
    @State var profilePicture: IImage?
    
    var resolution = 50.0
    var hideProgress = false
    
    var body: some View {
        if let pic = profilePicture {
            if case MyImageEnum.remote(let url) = pic.src {
                AsyncImage(url: URL(string: url)!) { image in
                    image
                        .resizable()
                        .aspectRatio(1.0, contentMode: .fill)
                        .frame(width: resolution, height: resolution)
                        .cornerRadius(100)
                } placeholder: {
                    ZStack {
                        Circle()
                            .foregroundColor(.gray).brightness(0.33)
                            .frame(width: resolution, height: resolution)

                        if !hideProgress {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                        }
                    }
                }
            } else if case MyImageEnum.local(let img) = pic.src {
                img
                    .resizable()
                    .aspectRatio(1.0, contentMode: .fill)
                    .frame(width: resolution, height: resolution)
                    .cornerRadius(100)
            }
        } else {
            Circle()
                .foregroundColor(.gray).brightness(0.33)
                .frame(width: resolution, height: resolution)
        }
    }
}

struct ProfilePicView_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePicView()
    }
}

//
//  NewPortfolioView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 12.03.2022.
//

import SwiftUI

struct NewPortfolioView: View {
    @EnvironmentObject var profileViewModel: ProfileViewModel
    
    @Environment(\.dismiss) private var dismiss
    
    @State private var name = ""
    @State private var description = "..."
    @State private var photos: [IImage] = []
    @State private var tags: [String] = []
    @State private var tagInput = ""
    
    @State private var newPortfolioImage: UIImage?
    @State private var isPickerPresented = false
    
    @State private var cancelAlertShowing = false
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(alignment: .leading) {
                Text("Název")
                    .font(.system(size: 18))
                
                ZStack {
                    RoundedRectangle(cornerRadius: 7)
                        .foregroundColor(.gray).brightness(0.35)
                        .frame(height: 38)
                    
                    TextField("např. Portréty", text: $name)
                        .font(.system(size: 18))
                        .frame(height: 38)
                        .foregroundColor(.gray)
                        .offset(x: 9)
                }
                .padding(.trailing, 15)
                
                Text("Fotografie")
                    .font(.system(size: 18))
                
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHStack {
                        Button(action: { isPickerPresented = true }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 9)
                                    .fill(Color.gray).brightness(0.34)
                                    .aspectRatio(1.0, contentMode: .fit)
                                    .frame(width: 150, height: 150)
                                
                                Image(systemName: "plus")
                                    .resizable()
                                    .frame(width: 17, height: 17)
                                    .foregroundColor(.black).brightness(0.3)
                            }
                        }
                        
                        if photos.isEmpty {
                            RoundedRectangle(cornerRadius: 9)
                                .fill(Color.gray).brightness(0.39)
                                .aspectRatio(1.0, contentMode: .fit)
                                .frame(width: 150, height: 150)
                            
                            RoundedRectangle(cornerRadius: 9)
                                .fill(Color.gray).brightness(0.41)
                                .aspectRatio(1.0, contentMode: .fit)
                                .frame(width: 150, height: 150)
                        } else {
                            ForEach(Array(photos.enumerated()), id: \.1.id) { (index, img) in
                                ZStack(alignment: .topTrailing) {
                                    if case MyImageEnum.local(let image) = img.src {
                                        image
                                            .resizable()
                                            .aspectRatio(1.0, contentMode: .fill)
                                            .frame(width: 150, height: 150)
                                            .cornerRadius(CGFloat(9))
                                    } else {
                                        RoundedRectangle(cornerRadius: 9)
                                            .fill(.red)
                                            .aspectRatio(1.0, contentMode: .fit)
                                            .frame(width: 150, height: 150)
                                            .opacity(0.65)
                                    }
                                    
                                    Button(action: {
                                        withAnimation {
                                            let _ = photos.remove(at: index)
                                        }
                                    }) {
                                        ZStack {
                                            RoundedRectangle(cornerRadius: 9)
                                                .fill(.gray)
                                                .aspectRatio(1.0, contentMode: .fit)
                                                .frame(width: 40, height: 40)
                                                .opacity(0.55)
                                            
                                            Image(systemName: "xmark")
                                                .resizable()
                                                .frame(width: 17, height: 17)
                                                .foregroundColor(.red)
                                                .opacity(0.9)
                                                .padding()
                                        }
                                    }
                                }
                            }
                            .transition(.opacity)
                        }
                    }
                }
                
                VStack(alignment: .leading) {
                    Text("Popis")
                        .font(.system(size: 18))
                        .padding(.bottom, -2)
                        .padding(.top, 3)
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 7)
                            .foregroundColor(.gray).brightness(0.35)
                            .frame(height: 85)
                        
                        TextEditor(text: $description)
                            .font(.system(size: 16))
                            .frame(height: 85)
                            .lineSpacing(2)
                            .foregroundColor(.gray)
                            .padding(.leading, 9)
                            .padding(.top, 7)
                            .onTapGesture {
                                if description == "..." {
                                    withAnimation {
                                        description = ""
                                    }
                                }
                            }
                            .transition(.opacity)
                    }
                }
                .padding(.trailing, 15)
                
                Text("Tagy (max. 5)")
                
                if tags.count < 5 {
                    HStack {
                        ZStack {
                            Rectangle()
                               .foregroundColor(.gray).brightness(0.37)
                            
                            TextField("např. svatba", text: $tagInput)
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .padding()
                         }
                        .frame(height: 40)
                        .cornerRadius(7)
                        
                        Button(action: {
                            addTag()
                        }, label: {
                            Text("Přidat")
                                .padding(10)
                                .background(.red).brightness(0.5)
                                .foregroundColor(.white)
                                .cornerRadius(7)
                        })
                    }
                    .padding(.trailing, 15)
                    .transition(.opacity)
                }
                
                VStack(alignment: .leading) {
                    ForEach(Array(tags.enumerated()), id: \.1.self) { index, tag in
                        HStack {
                            Text(tag)
                                .padding([.leading, .trailing], 9)
                                .padding([.bottom, .top], 7)
                                .background(.gray).brightness(0.4)
                                .cornerRadius(7)
                            
                            Button(action: {
                                withAnimation {
                                    let _ = tags.remove(at: index)
                                }
                            }) {
                                Image(systemName: "xmark")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(width: 12, height: 12)
                                    .foregroundColor(.gray).brightness(0.15)
                            }
                        }
                    }
                    .transition(.opacity)
                }
                
                Spacer()
            }
        }
        .padding(.leading, 15)
        .onAppear(perform: {
            UITextView.appearance().backgroundColor = .clear
        })
        .sheet(isPresented: $isPickerPresented, onDismiss: {
            guard let newPortfolioImage = newPortfolioImage else { return }

            photos.append(IImage(src: MyImageEnum.local(Image(uiImage: newPortfolioImage))))
            self.newPortfolioImage = nil
        }, content: {
            ImagePicker(isPresented: $isPickerPresented, image: $newPortfolioImage)
        })
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarLeading) {
                Button(action: { cancelAlertShowing = true }) {
                    Text("Zrušit")
                }
                .foregroundColor(.gray)
            }
            
            ToolbarItem(placement: .principal) {
                Text("Nové portfolio")
                    .font(.headline)
                    .foregroundColor(.red).brightness(0.15)
            }
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: {
                    savePortfolio()
                    dismiss()
                }) {
                    Text("Vytvořit")
                }
                .foregroundColor(.gray)
            }
        }
        .navigationBarBackButtonHidden(true)
        .alert("Opravdu chcete zrušit tvorbu portfolia?", isPresented: $cancelAlertShowing) {
            Button("Ne") { cancelAlertShowing = false }
            
            Button("Ano") { dismiss() }
        }
    }
    
    private func savePortfolio() {
        if photos.isEmpty || name.isEmpty {
            return
        }
        
        guard let user = profileViewModel.user else { return }
        
        let portfolio = Portfolio(id: UUID(), authorUsername: user.username, name: name, photos: photos, description: description, tags: tags, timestamp: .now)
        
        profileViewModel.portfolios.append(portfolio)
    }
    
    private func addTag() {
        if !tags.contains(tagInput) && tags.count < 5 {
            withAnimation {
                tags.append(tagInput.replacingOccurrences(of: " ", with: "").lowercased())
                tagInput = ""
            }
        }
    }
}

struct NewPortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            NewPortfolioView(profileViewModel: .init())
        }
    }
}

//
//  RatingView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 13.03.2022.
//

import SwiftUI

struct RatingView: View {
    @AppStorage("username") var username: String = "default"
    @Environment(\.dismiss) private var dismiss
    
    @Binding var ratings: [String:Int]
    
    @State var rating = 5
    
    var body: some View {
        VStack {
            HStack {
                ForEach(0..<5) { index in
                    Image(systemName: "star.fill")
                        .resizable()
                        .aspectRatio(1, contentMode: .fit)
                        .frame(width: 30)
                        .foregroundColor(index + 1 > rating ? .gray : .yellow)
                        .onTapGesture {
                            withAnimation {
                                rating = index + 1
                            }
                        }
                }
            }
            .padding(.bottom, 5)
            
            Button(action: {
                ratings[username] = rating
                dismiss()
            }, label: {
                Text("Udělit hodnocení")
                    .foregroundColor(.black)
                    .padding([.top, .bottom], 9)
                    .padding([.leading, .trailing], 9)
                    .background(.gray).brightness(0.3)
                    .cornerRadius(9)
            })
        }
        .onAppear(perform: {
            if let current = ratings[username] { rating = current }
        })
        .navigationTitle("Hodnocení")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RatingView(ratings: .constant([:]))
        }
    }
}

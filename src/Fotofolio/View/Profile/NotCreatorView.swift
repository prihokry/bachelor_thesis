//
//  NotCreatorView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 16.03.2022.
//

import SwiftUI

struct NotCreatorView: View {
    var body: some View {
        VStack {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.35)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                        
                        Text("Uživatel")
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.2)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                        
                        Text("není")
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.15)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                            .padding(.trailing, 10)
                        
                        Text("tvůrce.")
                    }
                }
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.31)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                        
                        Text("Žádná")
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.29)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                            .padding(.trailing, 10)
                        
                        Text("portfolia")
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.4)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                        
                        Text("k")
                    }
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 9)
                            .fill(Color.gray).brightness(0.29)
                            .aspectRatio(1.0, contentMode: .fit)
                            .frame(width: 150, height: 150)
                            .padding(.leading, 5)
                            .padding(.trailing, 10)
                        
                        Text("zobrazení.")
                    }
                    
                    Spacer()
                }
            }
            .padding(.top, 5)
        }
        .padding(.leading, 25)
        .padding(.top, 5)
    }
}

struct NotCreatorView_Previews: PreviewProvider {
    static var previews: some View {
        NotCreatorView()
    }
}

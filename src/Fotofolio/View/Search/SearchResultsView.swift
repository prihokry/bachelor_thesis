//
//  SearchResultsView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 21.02.2022.
//

import SwiftUI

struct SearchResultsView: View {
    @Binding var results: [User]
    
    var body: some View {
        ForEach(results, id: \.id) { user in
            NavigationLink(destination: ProfileView(profileViewModel: .init(), user: user.username, signOutClear: .constant(false))) {
                HStack {
                    ProfilePicView(profilePicture: user.profilePicture, resolution: 60.0)
                    
                    VStack(alignment: .leading, spacing: 0) {
                        Text(user.username)
                            .font(.system(size: 16))
                            .foregroundColor(.black).brightness(0.2)
                            .padding(.bottom, 4)
                        
                        Text(user.location)
                            .font(.system(size: 13))
                            .foregroundColor(.black).brightness(0.3)
                            .padding(.bottom, 2)
                    }
                    .padding(.leading, 7)
                    
                    Spacer()
                }
                .padding(.top, 12)
                .transition(.move(edge: .trailing))
            }
        }
    }
}

struct SearchResultsView_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultsView(results: .constant([User.dummy1]))
    }
}

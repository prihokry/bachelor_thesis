//
//  SearchView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 10.02.2022.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject var searchViewModel: SearchViewModel = .init()
    
    @AppStorage("username") var username: String = "default"
    
    var body: some View {
        ScrollView {
            VStack {
                Picker("Vyhledávat podle", selection: $searchViewModel.searchMode) {
                    ForEach(0..<searchViewModel.searchOptions.count) {
                        Text(searchViewModel.searchOptions[$0])
                    }
                }
                .pickerStyle(.segmented)
                .padding(.top, 5)
                
                ZStack {
                    Rectangle()
                       .foregroundColor(.gray).brightness(0.37)
                    
                    HStack {
                        Image(systemName: "magnifyingglass")
                        TextField("Hledat", text: $searchViewModel.searchString) { editing in
                            if editing {
                                searchViewModel.searching = true
                            }
                        } onCommit: {
                            searchViewModel.searching = false
                        }
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                    }
                    .padding()
                 }
                .frame(height: 45)
                .cornerRadius(10)
                
                SearchResultsView(results: $searchViewModel.searchedUsers)
            }
        }
        .padding([.leading, .trailing])
        .navigationTitle("Vyhledávání")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            if searchViewModel.searching {
                Button("Zrušit") {
                    searchViewModel.searchString = ""
                    searchViewModel.searching = false
                    
                    UIApplication.shared.dismissKeyboard()
                }
                .transition(.opacity)
            }
        }
        .onChange(of: searchViewModel.searchString, perform: { _ in
            withAnimation {
                searchViewModel.searchedUsers = searchViewModel.fetchUsersBySubstring(input: searchViewModel.searchString, mode: searchViewModel.searchMode)
            }
        })
        .gesture(DragGesture()
                    .onChanged({ _ in
                        UIApplication.shared.dismissKeyboard()
                        
                        withAnimation {
                            searchViewModel.searching = false
                        }
                    })
        )
    }
}

extension UIApplication {
    func dismissKeyboard() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SearchView(searchViewModel: .init())
        }
    }
}

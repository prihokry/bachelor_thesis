//
//  TabEnum.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 10.04.2022.
//

import Foundation

enum TabEnum {
    case feed
    case selection
    case search
    case messages
    case profile
}

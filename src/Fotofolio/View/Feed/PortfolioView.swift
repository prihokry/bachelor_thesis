//
//  PortfolioView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 29.01.2022.
//

import SwiftUI

struct PortfolioView: View {
    @EnvironmentObject var portfolioViewModel: PortfolioViewModel
    let portfolio: Portfolio
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {}, label: {
                    NavigationLink(destination: ProfileView(profileViewModel: .init(), user: portfolio.authorUsername, signOutClear: .constant(false))) {
                        Text("@" + portfolio.authorUsername)
                            .font(.title2)
                            .padding(.leading, 20)
                            .foregroundColor(.pink)
                    }
                })
                
                Spacer()
                
                Button(action: {
                    portfolioViewModel.flagged.contains(portfolio)  ?
                    portfolioViewModel.removeFromFlagged(portfolio) : portfolioViewModel.addToFlagged(portfolio)
                }) {
                    Image(systemName: portfolioViewModel.flagged.contains(portfolio) ? "bookmark.fill" : "bookmark")
                        .font(.title3)
                        .foregroundColor(portfolioViewModel.flagged.contains(portfolio) ? .red : .gray)
                        .transition(.opacity)
                }
                .padding(.trailing, 20)
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHStack {
                    ForEach(Array(portfolio.photos.enumerated()), id: \.1.id) { index, photo in
                        if case MyImageEnum.remote(let url) = photo.src {
                            AsyncImage(url: URL(string: url)!) { image in
                                image
                                    .resizable()
                                    .aspectRatio(1.0, contentMode: .fill)
                                    .frame(width: 350, height: 350)
                                    .cornerRadius(CGFloat(9))
                                    .padding(.leading, 13)
                                    .padding(.trailing, index == portfolio.photos.count - 1 ? 13 : 0)
                            } placeholder: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 9)
                                        .fill(Color.gray).brightness(0.25)
                                        .aspectRatio(1.0, contentMode: .fill)
                                        .frame(width: 350, height: 350)
                                        .padding(.leading, 13)

                                    ProgressView()
                                        .progressViewStyle(CircularProgressViewStyle())
                                }
                            }
                        } else if case MyImageEnum.local(let image) = photo.src {
                            image
                                .resizable()
                                .aspectRatio(1.0, contentMode: .fill)
                                .frame(width: 350, height: 350)
                                .cornerRadius(CGFloat(9))
                                .padding(.leading, 13)
                                .padding(.trailing, index == portfolio.photos.count - 1 ? 13 : 0)
                        }
                    }
                }
            }
            .onTapGesture(count: 2) {
                if (!portfolioViewModel.flagged.contains(portfolio)) {
                    portfolioViewModel.addToFlagged(portfolio)
                }
            }
            
            HStack {
                Text(portfolio.description)
                    .foregroundColor(Color(uiColor: UIColor.systemGray))
                
                Spacer()
            }
            .padding(.top, 5)
            .padding([.leading, .trailing, .bottom])
        }
        .transition(.opacity)
    }
}

struct PortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioView(portfolio: Portfolio.dummyPortfolio1)
            .environmentObject(PortfolioViewModel.init())
    }
}

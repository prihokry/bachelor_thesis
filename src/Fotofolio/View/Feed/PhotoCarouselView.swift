//
//  PhotoCarouselView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 09.02.2022.
//

import SwiftUI

struct PhotoCarouselView: View {
    var photos: [IImage]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(Array(photos.enumerated()), id: \.1.id) { index, photo in
                    if case MyImageEnum.remote(let url) = photo.src {
                        AsyncImage(url: URL(string: url)!) { image in
                            image
                                .resizable()
                                .aspectRatio(1.0, contentMode: .fill)
                                .frame(width: 350, height: 350)
                                .cornerRadius(CGFloat(9))
                                .padding(.leading, 13)
                                .padding(.trailing, index == photos.count - 1 ? 13 : 0)
                        } placeholder: {
                            ZStack {
                                RoundedRectangle(cornerRadius: 9)
                                    .fill(Color.gray).brightness(0.25)
                                    .aspectRatio(1.0, contentMode: .fill)
                                    .frame(width: 350, height: 350)
                                    .padding(.leading, 13)

                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle())
                            }
                        }
                    } else if case MyImageEnum.local(let image) = photo.src {
                        image
                            .resizable()
                            .aspectRatio(1.0, contentMode: .fill)
                            .frame(width: 350, height: 350)
                            .cornerRadius(CGFloat(9))
                            .padding(.leading, 13)
                            .padding(.trailing, index == photos.count - 1 ? 13 : 0)
                    }
                }
            }
        }
    }
}

struct PhotoCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoCarouselView(photos: [IImage(src: MyImageEnum.remote("https://placeimg.com/400/400/architecture"))])
    }
}

//
//  FeedView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 28.01.2022.
//

import SwiftUI

struct FeedView: View {
    @EnvironmentObject var portfolioViewModel: PortfolioViewModel
    
    @State var showFilter = false
    @State var usedFilter = false
    @State var arrowAngle = 0.0
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(showsIndicators: false) {
                if (portfolioViewModel.portfolios.isEmpty) {
                    if usedFilter {
                        VStack(alignment: .center) {
                            Text("Filtrování neodpovídá žádný výsledek.")
                                .foregroundColor(.red)
                                .frame(width: geometry.size.width)
                                .frame(minHeight: geometry.size.height)
                        }
                    } else {
                        VStack(alignment: .center) {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                                .frame(width: geometry.size.width)
                                .frame(minHeight: geometry.size.height)
                        }
                    }
                } else {
                    VStack {
                        ForEach(portfolioViewModel.portfolios, id: \.id) { portfolio in
                            PortfolioView(portfolio: portfolio)
                        }
                    }
                }
            }
        }
        .onAppear(perform: {
            if portfolioViewModel.portfolios.isEmpty {
                portfolioViewModel.fetchPortfolios()
            }
        })
        .navigationTitle("Feed")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItemGroup(placement: .navigationBarLeading) {
                Button(action: {
                    portfolioViewModel.fetchPortfolios()
                    arrowAngle -= 720
                    
                    portfolioViewModel.tags = []
                    usedFilter = false
                }) {
                    Image(systemName: "arrow.counterclockwise")
                        .foregroundColor(.black).brightness(0.4)
                        .rotationEffect(.degrees(arrowAngle))
                        .animation(.easeInOut(duration: 1.7), value: arrowAngle)
                }
                .padding(.leading, 5)
                .padding(.trailing, -14)
                
                Menu("Seřadit") {
                    Button("Podle data", action: { withAnimation { portfolioViewModel.sortPortfoliosByDate() } })
                    Button("Podle hodnocení", action: { withAnimation { portfolioViewModel.sortPortfoliosByRating() } })
                }
                .foregroundColor(.gray)
                .padding(.leading, 0)
            }
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: { showFilter = true }) {
                    Text("Filtrovat")
                        .foregroundColor(.gray)
                }
                .padding(.trailing, 5)
            }
        }
        .sheet(isPresented: $showFilter, content: {
            FilterView(showFilter: $showFilter, usedFilter: $usedFilter)
        })
    }
}

struct FeedView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            FeedView()
                .environmentObject(PortfolioViewModel.init())
        }
    }
}

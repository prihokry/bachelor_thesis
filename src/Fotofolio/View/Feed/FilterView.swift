//
//  FilterView.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 11.03.2022.
//

import SwiftUI

struct FilterView: View {
    @EnvironmentObject var portfolioViewModel: PortfolioViewModel
    
    @State var textInput: String = ""
    @Binding var showFilter: Bool
    @Binding var usedFilter: Bool
    
    var body: some View {
        VStack {
            ZStack {
                Text("Filtrovat portfolia")
                    .fontWeight(.medium)
                
                HStack {
                    Spacer()
                    
                    Button(action: {
                        if !portfolioViewModel.tags.isEmpty {
                            portfolioViewModel.filterPortfolios()
                        }
                        
                        showFilter = false
                    }) {
                        Text("Zavřít")
                            .foregroundColor(.red).brightness(0.2)
                        
                        Image(systemName: "xmark")
                            .resizable()
                            .aspectRatio(1, contentMode: .fit)
                            .frame(width: 12, height: 12)
                            .foregroundColor(.red).brightness(0.2)
                    }
                    .padding()
                }
            }
            
            HStack {
                ZStack {
                    Rectangle()
                       .foregroundColor(.gray).brightness(0.37)
                    
                    TextField("např. svatba", text: $textInput)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .padding()
                 }
                .frame(height: 40)
                .cornerRadius(7)
                .padding(.leading)
                
                Button(action: {
                    if portfolioViewModel.addTag(textInput) { usedFilter = true }
                    
                    textInput = ""
                }, label: {
                    Text("Přidat")
                        .padding(10)
                        .background(.red).brightness(0.5)
                        .foregroundColor(.white)
                        .cornerRadius(7)
                })
                    .padding(.trailing)
            }
            
            HStack {
                VStack(alignment: .leading) {
                    ForEach(portfolioViewModel.tags, id: \.self) { tag in
                        HStack {
                            Text(tag)
                                .padding([.leading, .trailing], 9)
                                .padding([.bottom, .top], 7)
                                .background(.gray).brightness(0.4)
                                .cornerRadius(7)
                            
                            Button(action: { withAnimation { portfolioViewModel.removeTag(tag) } }) {
                                Image(systemName: "xmark")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(width: 12, height: 12)
                                    .foregroundColor(.gray).brightness(0.15)
                            }
                        }
                    }
                    .transition(.opacity)
                }
                .padding(.leading)
                
                Spacer()
            }
            
            Spacer()
        }
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView(showFilter: .constant(true), usedFilter: .constant(true))
            .environmentObject(PortfolioViewModel.init())
    }
}

//
//  FotofolioApp.swift
//  Fotofolio
//
//  Created by Kryštof Příhoda on 27.01.2022.
//

import SwiftUI

@main
struct FotofolioApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .preferredColorScheme(.light)
        }
    }
}
